
@extends('members.front')

@section('content')
<div class = "card">

	<h1>Create a member</h1>
	<form method="post" action="/cinema/members/store">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-4">
				<label for="name">Name</label>
			</div>
			<div class = "col-8">
				<input type="text" name ="name"  id ="name"  value="<?php echo old('name');?>">
			</div>
		</div>
		<div class="row">
			<div class="col-4">
				<label for="name">Email</label>
			</div>
			<div class = "col-8">
				<input type="email" id ="name" name="emailaddress">
			</div>
		</div>

		<div>
			<button type="submit">Create a member</button>
		</div>
		
	</form>
</div>
@endsection