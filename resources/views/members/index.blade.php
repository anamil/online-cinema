<!DOCTYPE html>
<html>
<head>
	<title>Index</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<div class = "container bg-light p-5 my-5 border border-info rounded-sm">
    <div class="d-flex justify-content-center display-4 mb-5">Members</div>
  </div>
	<div class="container border border-info">
		<table class="table table-striped table-hover p-5 my-5 border border-info">
			<tr class="table-dark">
				<th>Name</th>
				<th>Email</th>
				<th>Password</th>
				<th>Type</th>
			</tr>	
			@foreach($members as $member)
			<tr>
				<td>{{$member->name}}</td>
				<td>{{$member->email}}</td>
				<td>{{$member->password}}</td>
				<td>{{$member->type}}</td>
			</tr>
			@endforeach
		</table>
	</div>
</body>
</html>
<table>
	