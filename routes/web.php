<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Route::get('/', function () {
    //return view('welcome');
// });

Route::prefix('members')->group(function () {
	Route::get('/index', 'MemberController@index');
	Route::get('/create','MemberController@create');
	Route::post('/store','MemberController@store');

});

Route::get('/members/{id}','MemberController@show');

